<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-bookmark
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('MENUITEM', 'myportfolio/bookmarks');

require(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('editbookmark', 'artefact.bookmark'));
require_once('pieforms/pieform.php');
safe_require('artefact', 'bookmark');
safe_require('artefact', 'file');

$bookmark = new ArtefactTypeBookmark(param_integer('id', 0));
$bookmark->set('dirty', false);
$id = $bookmark->get('id');
$graphic = $bookmark->get('graphic');

$folder = param_integer('folder', 0);
$browse = (int) param_variable('browse', 0);
$highlight = null;
if ($file = param_integer('file', 0)) {
    $highlight = array($file);
}


$elements = array(
    'url' => array(
        'type'          => 'text',
        'title'         => get_string('URL', 'artefact.bookmark'),
        'size'          => 60,
        'rules' => array(
            'required'    => true
        ),
        'defaultvalue'  => $bookmark->get('url')
    ),
    'title' => array(
        'type'          => 'text',
        'title'         => get_string('name'),
        'size'          => 60,
        'rules' => array(
            'required'    => true
        ),
        'defaultvalue'  => $bookmark->get('title')
    ),
    'description' => array(
        'type'          => 'textarea',
        'rows'          => 5,
        'cols'          => 65,
        'title'         => get_string('Comment', 'artefact.bookmark'),
        'defaultvalue'  => $bookmark->get('description')
    ),
    'tags'       => array(
        'defaultvalue' => $bookmark->get('tags'),
        'type'         => 'tags',
        'title'        => get_string('tags'),
        'description'  => get_string('tagsdescprofile'),
        'help' => true,
    ),
    'filebrowser' => array(
        'type'         => 'filebrowser',
        'title'        => get_string('graphic', 'artefact.bookmark'),
        'folder'       => $folder,
        'highlight'    => $highlight,
        'browse'       => $browse,
        'page'         => get_config('wwwroot') . 'artefact/bookmark/edit.php?browse=1' . ($id ? ('&id=' . $id) : ''),
        'browsehelp'   => 'browsemyfiles',
        'filters'      => array('image'),
        'config'       => array(
            'upload'          => true,
            'uploadagreement' => get_config_plugin('artefact', 'file', 'uploadagreement'),
            'createfolder'    => false,
            'edit'            => false,
            'select'          => true,
            'selectone'       => true,
        ),
        'defaultvalue'       => $graphic ? array($graphic) : array(),
        'selectlistcallback' => 'artefact_get_records_by_id',
        'selectcallback'     => 'add_attachment',
        'unselectcallback'   => 'delete_attachment',
    ),
    'submitbookmark' => array(
        'type'  => 'submitcancel',
        'value' => array(get_string('save'), get_string('cancel')),
        'goto'  => get_config('wwwroot') . 'artefact/bookmark',
    ),
);

$form = pieform(array(
    'name'              => 'editbookmark',
    'jsform'            => true,
    'newiframeonsubmit' => true,
    'plugintype'        => 'artefact',
    'pluginname'        => 'bookmark',
    'jssuccesscallback' => 'editbookmark_success',
    'configdirs'        => array(get_config('libroot') . 'form/', get_config('docroot') . 'artefact/file/form/'),
    'elements'          => $elements,
));

$javascript = <<<EOF
function editbookmark_success(form, data) {
    editbookmark_filebrowser.success(form, data);
};
function editbookmark_error(form, data) {
    editbookmark_filebrowser.init();
};
EOF;

$smarty = smarty();
$smarty->assign('INLINEJAVASCRIPT', $javascript);
$smarty->assign_by_ref('editform', $form);
$smarty->assign_by_ref('bookmark', $bookmark);
$smarty->display('artefact:bookmark:edit.tpl');
exit;

function editbookmark_submit(Pieform $form, $values) {
    global $SESSION, $USER, $bookmark;

    db_begin();

    foreach (array('url', 'title', 'description', 'tags') as $k) {
        $bookmark->set($k, $values[$k]);
    }
    $id = $bookmark->get('id');
    if (!$id) {
        $bookmark->set('owner', $USER->get('id'));
    }
    $bookmark->commit();

    // Attachments
    $old = $bookmark->attachment_id_list();
    $oldid = empty($old) ? null : $old[0];
    $newid = $values['filebrowser'];
    if ($oldid != $newid) {
        foreach ($old as $o) {
            $bookmark->detach($o);
        }
        if ($newid) {
            $bookmark->attach($newid);
        }
    }

    db_commit();

    $submitelement = $form->get_element('submitbookmark');
    $result = array(
        'error'   => false,
        'message' => get_string('bookmarksaved', 'artefact.bookmark'),
        'goto'    => $submitelement['goto'],
    );

    if ($form->submitted_by_js()) {
        // Redirect back to the bookmark list from within the iframe
        $SESSION->add_ok_msg($result['message']);
        $form->json_reply(PIEFORM_OK, $result, false);
    }
    $form->reply(PIEFORM_OK, $result);
}

?>
