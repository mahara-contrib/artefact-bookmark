<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-bookmark
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

class PluginArtefactBookmark extends PluginArtefact {

    public static function get_artefact_types() {
        return array(
            'bookmark',
        );
    }
    
    public static function get_block_types() {
        return array();
    }

    public static function get_plugin_name() {
        return 'bookmark';
    }

    public static function menu_items() {
        return array(
            array(
                'path' => 'myportfolio/bookmarks',
                'url' => 'artefact/bookmark/',
                'title' => get_string('mybookmarks', 'artefact.bookmark'),
                'weight' => 40,
            ),
        );
    }

    public static function get_event_subscriptions() {
        return array();
    }

}

class ArtefactTypeBookmark extends ArtefactType {

    protected $url;
    protected $graphic;

    public function __construct($id = 0, $data = null) {
        parent::__construct($id, $data);
        $this->url = $this->note;
        if ($this->id) {
            if ($attachments = $this->attachment_id_list()) {
                $this->graphic = $attachments[0];
            }
        }
    }

    public function commit() {
        $this->note = $this->url;
        parent::commit();
    }

    public static function is_singular() {
        return false;
    }

    public static function get_icon($options=null) {
        global $THEME;

        if (!empty($options['imageid'])) {
            safe_require('artefact', 'file');
            $options['id'] = $options['imageid'];
            unset($options['imageid']);
            return ArtefactTypeImage::get_icon($options);
        }

        return $THEME->get_url('images/icon-web.gif');
    }

    public function delete() {
        if (empty($this->id)) {
            return; 
        }
        db_begin();
        $this->detach();
        parent::delete();
        db_commit();
    }

    public static function get_links($id) {
        return array(
            '_default' => get_config('wwwroot') . 'artefact/bookmark/view.php?id=' . $id,
        );
    }

    public function can_have_attachments() {
        return true;
    }

    public function to_stdclass($options) {
        $options['imageid'] = $this->graphic;
        return (object) array(
            'id'          => $this->id,
            'name'        => $this->title,
            'url'         => $this->url,
            'description' => $this->description,
            'created'     => $this->ctime,
            'icon'        => ArtefactTypeBookmark::get_icon($options),
        );
    }

    public function render_self($options) {
        $smarty = smarty_core();
        $smarty->assign('bookmark', $this->to_stdclass($options));
        return array('html' => $smarty->fetch('artefact:bookmark:bookmark.tpl'), 'javascript' => null);
    }

    public static function get_bookmarks() {
        global $USER;
        safe_require('artefact', 'file');
        if (!$records = get_records_sql_assoc('
            SELECT a.*, aa.attachment AS imageid
            FROM {artefact} a LEFT JOIN {artefact_attachment} aa ON a.id = aa.artefact
            WHERE owner = ? AND artefacttype = ?', array($USER->get('id'), 'bookmark'))) {
            return array();
        }
        if ($tags = get_records_select_array('artefact_tag', 'artefact IN (' . join(',', array_keys($records)) . ')')) {
            foreach ($tags as &$t) {
                $records[$t->artefact]->tags[] = $t->tag;
            }
        }
        foreach ($records as &$r) {
            $r->icon = self::get_icon(array('imageid' => $r->imageid));
        }
        return $records;
    }
}

?>
