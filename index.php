<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-bookmark
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('MENUITEM', 'myportfolio/bookmarks');

require(dirname(dirname(dirname(__FILE__))) . '/init.php');
safe_require('artefact', 'bookmark');

if ($delete = param_integer('delete', 0)) {
    $a = artefact_instance_from_id($delete);
    if ($USER->can_edit_artefact($a)) {
        $a->delete();
    }
}

$bookmarks = ArtefactTypeBookmark::get_bookmarks();

define('TITLE', get_string('bookmarksfor', 'artefact.bookmark', display_name($USER, null, true)));

$smarty = smarty();
$smarty->assign('bookmarks', $bookmarks);
$smarty->display('artefact:bookmark:index.tpl');

?>
