{include file="header.tpl"}
{include file="sidebar.tpl"}


<div><a href="{$WWWROOT}artefact/bookmark/edit.php">{str tag="addabookmark" section="artefact.bookmark"}</a></div>
{if $bookmarks}
<table class="tablerenderer">
 <thead>
  <tr>
   <th></th>
   <th>{str tag=URL section=artefact.bookmark}</th>
   <th>{str tag=name}</th>
   <th>{str tag=description}</th>
   <th></th>
  </tr>
 </thead>
 <tbody>
 {foreach from=$bookmarks item=bookmark}
  <tr>
   <td><img src="{$bookmark->icon}" alt="{str tag=bookmarkicon section=artefact.bookmark}"></td>
   <td><a href="{$bookmark->note|escape}" target="_blank">{$bookmark->note|escape}</a></td>
   <td><a href="{$WWWROOT}artefact/bookmark/view.php?id={$bookmark->id}">{$bookmark->title|escape}</a></td>
   <td>{$bookmark->description|str_shorten_text:34|escape}</td>
   <td>
     <a href="{$WWWROOT}artefact/bookmark/edit.php?id={$bookmark->id}">{str tag=edit}</a>
     <a href="{$WWWROOT}artefact/bookmark/index.php?delete={$bookmark->id}">{str tag=delete}</a>
   </td>
  </tr>
 {/foreach}
 </tbody>
</table>
{else}
<div>{str tag="nobookmarks" section="artefact.bookmark"}</div>
{/if}
{include file="footer.tpl"}
