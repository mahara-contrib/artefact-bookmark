<div style="float:left; margin-right:5px;">
  <img src="{$bookmark->icon}">
</div>
<div><strong>{$bookmark->name|escape}</strong></div>
<div><a href="{$bookmark->url|escape}" target="_blank">{$bookmark->url|escape}</a></div>
<div>{$bookmark->description|escape}</div>
